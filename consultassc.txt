SELECT DISTINCT causa_fallecimiento FROM dim_causa INNER JOIN hechos_mortalidad ON dim_causa.id_causa = hechos_mortalidad.id_causa;
SELECT causa_fallecimiento, COUNT(causa_fallecimiento) as total FROM dim_causa INNER JOIN hechos_mortalidad ON dim_causa.id_causa = hechos_mortalidad.id_causa GROUP BY causa_fallecimiento;

SELECT DISTINCT genero_fallecido FROM dim_genero INNER JOIN hechos_mortalidad ON dim_genero.id_genero = hechos_mortalidad.id_genero;
SELECT genero_fallecido, COUNT(genero_fallecido) as total FROM dim_genero INNER JOIN hechos_mortalidad ON dim_genero.id_genero = hechos_mortalidad.id_genero GROUP BY genero_fallecido;

SELECT DISTINCT genero_fallecido FROM dim_genero INNER JOIN hechos_mortalidad ON dim_genero.id_genero = hechos_mortalidad.id_genero;
SELECT dim_genero.genero_fallecido, COUNT(dim_causa.causa_fallecimiento) as total 
FROM hechos_mortalidad  
INNER JOIN dim_genero ON hechos_mortalidad.id_genero = dim_genero.id_genero 
INNER JOIN dim_causa ON hechos_mortalidad.id_causa = dim_causa.id_causa
WHERE dim_causa.causa_fallecimiento='covid'
GROUP BY dim_genero.genero_fallecido;

SELECT DISTINCT EXTRACT(YEAR FROM fecha_fallecimiento) as cantidad_años FROM dim_fecha INNER JOIN hechos_mortalidad ON dim_fecha.id_fecha = hechos_mortalidad.id_fecha ORDER BY cantidad_años;
SELECT DISTINCT EXTRACT(YEAR FROM fecha_fallecimiento)AS fecha, COUNT(EXTRACT (YEAR FROM fecha_fallecimiento)) AS total FROM dim_fecha INNER JOIN hechos_mortalidad ON dim_fecha.id_fecha = hechos_mortalidad.id_fecha GROUP BY fecha;

SELECT * FROM hechos_mortalidad 
INNER JOIN dim_causa ON dim_causa.id_causa = hechos_mortalidad.id_causa
INNER JOIN dim_genero ON dim_genero.id_genero = hechos_mortalidad.id_genero
INNER JOIN dim_fecha ON dim_fecha.id_fecha = hechos_mortalidad.id_fecha;