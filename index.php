<?php
require_once('conexion.php');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">

    <title>Datamart Funeraria</title>
  </head>
  <body>
    <div class="col-lg-12" style="padding-top: 20px;">
      <div class="card">
        <h5 class="card-header">Tabla de Hechos</h5>
          <div class="card-body">
            <div class="container-fluid">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>No. Hecho</th>
                    <th>Causa</th>
                    <th>Genero</th>
                    <th>Fecha</th>
                  </tr>
              </thead>
              <tbody>
                <?php 
                  $sql="SELECT * FROM hechos_mortalidad 
INNER JOIN dim_causa ON dim_causa.id_causa = hechos_mortalidad.id_causa
INNER JOIN dim_genero ON dim_genero.id_genero = hechos_mortalidad.id_genero
INNER JOIN dim_fecha ON dim_fecha.id_fecha = hechos_mortalidad.id_fecha;";
                  $result=pg_query($conexion,$sql);
                  while($obj=pg_fetch_object($result)){
                ?>
                <tr>
                  <td><?php echo $obj->id_hecho;?></td>
                  <td><?php echo $obj->causa_fallecimiento;?></td>
                  <td><?php echo $obj->genero_fallecido;?></td>
                  <td><?php echo $obj->fecha_fallecimiento;?></td>
                </tr>
              <?php }?>
              </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  	<div class="col-lg-12" style="padding-top: 20px;">
    <div class="card">
  		<h5 class="card-header">Graficos Estadisticos del datamart de la Funeraria</h5>
  			<div class="card-body">
    			<div class="row">
            <div class="col-lg-2"> 
              <button class="btn btn-primary" onclick="CargarDatosGraficoBar();"> Grafico Causas</button>
            </div>
            <div class="col-lg-2">
            <button class="btn btn-primary" onclick="CargarDatosGraficoBar2();"> Grafico Genero</button>
            </div>
            <div class="col-lg-2">
            <button class="btn btn-primary" onclick="CargarDatosGraficoBar3();"> Grafico Covid</button>
            </div>
            <div class="col-lg-2">
            <button class="btn btn-primary" onclick="CargarDatosGraficoBar4();"> Grafico Tiempo</button>
            </div>
          </div>
          <br>
            <div class="row">
              <div class="col">
                <h4>Cantidad de Fallecidos(Categorizados)</h4>
                <canvas id="myChart"></canvas>
              </div>
              <div class="col">
                <h4>Cantidad de Fallecidos(Genero)</h4>
                <canvas id="myChart2"></canvas>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col">
                <h4>Cantidad de hombres y mujeres fallecidos por covid</h4>
                <canvas id="myChart3"></canvas>
              </div>
              <div class="col">
                <h4>Cantidad de fallecidos por año</h4>
                <canvas id="myChart4"></canvas>
              </div>
            </div>
  			</div>
	</div>
</div>

  </body>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <script type="text/javascript">
      function CargarDatosGraficoBar(){
      
      const ctx = document.getElementById('myChart');
      const myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
              labels: [
              <?php
              $sql="SELECT DISTINCT causa_fallecimiento FROM dim_causa INNER JOIN hechos_mortalidad ON dim_causa.id_causa = hechos_mortalidad.id_causa;";
              $result=pg_query($conexion,$sql);
              while ($registros = pg_fetch_array($result)){
              ?>
              '<?php echo $registros["causa_fallecimiento"]?>',
              <?php   
              }
              ?>
              ],
              datasets: [{
                  label: 'Numero de fallecidos',
                  data: 
                  <?php
                  $sql="SELECT causa_fallecimiento, COUNT(causa_fallecimiento) as total FROM dim_causa INNER JOIN hechos_mortalidad ON dim_causa.id_causa = hechos_mortalidad.id_causa GROUP BY causa_fallecimiento;";
                  $result = pg_query($conexion,$sql);
                  ?>
                  [<?php while ($registros = pg_fetch_array($result)) {
                  ?>
                  <?php echo $registros["total"]?>, 
                  <?php } ?>
                  ],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  y: {
                      beginAtZero: true
                  }
              }
          }
      });
}
function CargarDatosGraficoBar2(){
      const ctx = document.getElementById('myChart2');
      const myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: [
              <?php
              $sql="SELECT DISTINCT genero_fallecido FROM dim_genero INNER JOIN hechos_mortalidad ON dim_genero.id_genero = hechos_mortalidad.id_genero;";
              $result=pg_query($conexion,$sql);
              while ($registros = pg_fetch_array($result)){
              ?>
              '<?php echo $registros["genero_fallecido"]?>',
              <?php   
              }
              ?>
              ],
              datasets: [{
                  label: 'Numero de fallecidos',
                  data: 
                  <?php
                  $sql="SELECT genero_fallecido, COUNT(genero_fallecido) as total FROM dim_genero INNER JOIN hechos_mortalidad ON dim_genero.id_genero = hechos_mortalidad.id_genero GROUP BY genero_fallecido;";
                  $result = pg_query($conexion,$sql);
                  ?>
                  [<?php while ($registros = pg_fetch_array($result)) {
                  ?>
                  <?php echo $registros["total"]?>, 
                  <?php } ?>
                  ],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  y: {
                      beginAtZero: true
                  }
              }
          }
      });
}
function CargarDatosGraficoBar3() {
        const ctx = document.getElementById('myChart3');
      const myChart = new Chart(ctx, {
          type: 'pie',
          data: {
              labels: [
              <?php
              $sql="SELECT DISTINCT genero_fallecido FROM dim_genero INNER JOIN hechos_mortalidad ON dim_genero.id_genero = hechos_mortalidad.id_genero;";
              $result=pg_query($conexion,$sql);
              while ($registros = pg_fetch_array($result)){
              ?>
              '<?php echo $registros["genero_fallecido"]?>',
              <?php   
              }
              ?>
              ],
              datasets: [{
                  label: 'Numero de fallecidos',
                  data: 
                  <?php
                  $sql="SELECT dim_genero.genero_fallecido, COUNT(dim_causa.causa_fallecimiento) as total 
                  FROM hechos_mortalidad  
                  INNER JOIN dim_genero ON hechos_mortalidad.id_genero = dim_genero.id_genero 
                  INNER JOIN dim_causa ON hechos_mortalidad.id_causa = dim_causa.id_causa
                  WHERE dim_causa.causa_fallecimiento='covid'
                  GROUP BY dim_genero.genero_fallecido;";
                  $result = pg_query($conexion,$sql);
                  ?>
                  [<?php while ($registros = pg_fetch_array($result)) {
                  ?>
                  <?php echo $registros["total"]?>, 
                  <?php } ?>
                  ],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  y: {
                      beginAtZero: true
                  }
              }
          }
      });
}

function CargarDatosGraficoBar4(){
      const ctx = document.getElementById('myChart4');
      const myChart = new Chart(ctx, {
          type: 'line',
          data: {
              labels: [
              <?php
              $sql="SELECT DISTINCT EXTRACT(YEAR FROM fecha_fallecimiento) as cantidad_años FROM dim_fecha INNER JOIN hechos_mortalidad ON dim_fecha.id_fecha = hechos_mortalidad.id_fecha ORDER BY cantidad_años;
";
              $result=pg_query($conexion,$sql);
              while ($registros = pg_fetch_array($result)){
              ?>
              '<?php echo $registros["cantidad_años"]?>',
              <?php   
              }
              ?>
              ],
              datasets: [{
                  label: 'Numero de fallecidos',
                  data: 
                  <?php
                  $sql="SELECT DISTINCT EXTRACT(YEAR FROM fecha_fallecimiento)AS fecha, COUNT(EXTRACT (YEAR FROM fecha_fallecimiento)) AS total FROM dim_fecha INNER JOIN hechos_mortalidad ON dim_fecha.id_fecha = hechos_mortalidad.id_fecha GROUP BY fecha;

";
                  $result = pg_query($conexion,$sql);
                  ?>
                  [<?php while ($registros = pg_fetch_array($result)) {
                  ?>
                  <?php echo $registros["total"]?>, 
                  <?php } ?>
                  ],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  y: {
                      beginAtZero: true
                  }
              }
          }
      });
}
    </script>
</html>
